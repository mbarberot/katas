<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    public function testPrintOne()
    {
        $fizzBuzz = new FizzBuzz();

        $this->assertEquals("1", $fizzBuzz->printNumber(1));
    }

    public function testPrintFizz()
    {
        $fizzBuzz = new FizzBuzz();

        $this->assertEquals("fizz", $fizzBuzz->printNumber(3));
        $this->assertEquals("fizz", $fizzBuzz->printNumber(6));
    }

    public function testPrintBuzz()
    {
        $fizzBuzz = new FizzBuzz();

        $this->assertEquals("buzz", $fizzBuzz->printNumber(5));
        $this->assertEquals("buzz", $fizzBuzz->printNumber(10));
    }

    public function testPrintFizzBuzz()
    {
        $fizzbuzz = new FizzBuzz();

        $this->assertEquals("fizzbuzz", $fizzbuzz->printNumber(15));
        $this->assertEquals("fizzbuzz", $fizzbuzz->printNumber(30));
    }

}