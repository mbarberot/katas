import {module, test} from 'qunit';
import {TestContext} from "ember-test-helpers";
import {setupTest} from 'ember-qunit';
import FizzbuzzService from "fizzbuzz/services/fizzbuzz";

class FizzbuzzStub extends FizzbuzzService {
  result = 'FooBar';
  value?: number;

  execute(value: number) {
    this.value = value;
    return this.result;
  }
}

const setupServices = (context: TestContext): FizzbuzzStub => {
  context.owner.register('service:fizzbuzz', FizzbuzzStub);
  return context.owner.lookup('service:fizzbuzz');
}

module('Unit | Controller | fizzbuzz', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    setupServices(this)
    let controller = this.owner.lookup('controller:fizzbuzz');
    assert.ok(controller);
  });

  test('it execute fizzbuzz and print the result', function (assert) {
    let service = setupServices(this);
    let controller = this.owner.lookup('controller:fizzbuzz');

    controller.integer = 1;
    controller.onSubmit({
      preventDefault: () => {
      }
    } as any);

    assert.equal(controller.result, "FooBar");
    assert.equal(service.value, 1);
  });
});
