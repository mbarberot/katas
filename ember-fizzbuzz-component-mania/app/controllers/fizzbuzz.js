import Controller from '@ember/controller';
import {action} from '@ember/object';
import {tracked} from "@glimmer/tracking";

export default class FizzbuzzController extends Controller {
  @tracked integer = 0;
  @tracked result = '';
  @tracked submitted = false;

  @action
  onSubmit(result) {
    this.result = result;
    this.submitted = true;
  }
}
