import {
  create,
  clickable,
  fillable,
  visitable
} from 'ember-cli-page-object';

export default create({
  visit: visitable('/fizzbuzz'),
  root: { scope: '[data-test="fizzbuzz"]' },

  pickInteger: fillable('[data-test="fizzbuzz-picker"]'),
  submit: clickable('[data-test="fizzbuzz-submit"]'),

  result: { scope: '[data-test="fizzbuzz-result"]' },

  async pick(value: number): Promise<void> {
    await this.pickInteger(`${value}`);
    await this.submit();
  }

});
