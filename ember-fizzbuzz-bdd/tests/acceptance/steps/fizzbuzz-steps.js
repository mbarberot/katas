import { visit, click, fillIn } from '@ember/test-helpers';
import steps from './steps';

export default function(assert) {
  return steps(assert)
    .given('I access to fizzbuzz', async function() {
      await visit('/fizzbuzz');
    })
    .when('I choose $number', async function(number) {
      await fillIn('[data-test="fizzbuzz-picker"]', number);
      await click('[data-test="fizzbuzz-submit"]');
    })
    .then('I can see $result', function(result) {
      let actual = document.querySelector('[data-test="fizzbuzz-result"]').textContent;
      console.log(actual);
      assert.equal(actual, result);
    });
}
