<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class FizzBuzzController extends Controller
{
    public function printNumber(int $number): Response {

        $result = "";

        if($number % 3 == 0) {
            $result .= "fizz";
        }

        if($number % 5 == 0) {
            $result .= "buzz";
        }

        $result = strlen($result) == 0 ? "$number" : $result;

        return \response(["result" => $result]);
    }
}
