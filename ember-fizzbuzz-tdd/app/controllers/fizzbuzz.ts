import FizzbuzzService from "fizzbuzz/services/fizzbuzz";
import Controller from '@ember/controller';
import {tracked} from "@glimmer/tracking";
import {action} from '@ember/object';
import {inject as service} from '@ember/service';

export default class FizzbuzzController extends Controller {
  @service fizzbuzz!: FizzbuzzService;
  @tracked integer = 0;
  @tracked result = '';

  @action
  onSubmit(event: Event) {
    event.preventDefault();
    this.result = this.fizzbuzz.execute(this.integer);
  }
}
