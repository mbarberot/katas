import Controller from '@ember/controller';
import {tracked} from "@glimmer/tracking";
import {action} from "@ember/object";

const fizzbuzz = (integer) => {
  let result = "";

  if (integer % 3 === 0) {
    result += "Fizz";
  }

  if (integer % 5 === 0) {
    result += "Buzz";
  }

  if (result.length === 0) {
    return `${integer}`
  }

  return result;
}

export default class FizzbuzzController extends Controller {
  @tracked integer = 0;
  @tracked result = "";

  @action
  submit(event) {
    event.preventDefault();
    this.result = fizzbuzz(this.integer);
  }

}
