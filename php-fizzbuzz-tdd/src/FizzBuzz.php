<?php declare(strict_types=1);


final class FizzBuzz
{
    public function printNumber(int $number): string
    {
        $result = "";

        if($number % 3 == 0) {
            $result .= "fizz";
        }

        if($number % 5 == 0) {
            $result .= "buzz";
        }

        return strlen($result) == 0 ? "$number" : $result;
    }
}