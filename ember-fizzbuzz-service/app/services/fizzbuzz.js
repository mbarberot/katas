import Service from '@ember/service';

export default class FizzbuzzService extends Service {
  forValue(value) {
    let result = '';

    if (value !== 0) {
      if (value % 3 === 0) {
        result += 'Fizz';
      }
      if (value % 5 === 0) {
        result += 'Buzz';
      }
    }

    if (result.length === 0) {
      result = `${value}`;
    }

    return result;
  }
}
