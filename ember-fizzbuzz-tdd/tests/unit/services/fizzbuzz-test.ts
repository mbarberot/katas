import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import FizzbuzzService from "fizzbuzz/services/fizzbuzz";

module('Unit | Service | fizzbuzz', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let service: FizzbuzzService = this.owner.lookup('service:fizzbuzz');
    assert.ok(service);
  });

  test('it execute fizzbuzz and print Fizz', function(assert) {
    let service: FizzbuzzService = this.owner.lookup('service:fizzbuzz');
    assert.equal(service.execute(3), "Fizz");
  });

  test('it execute fizzbuzz and print Buzz', function(assert) {
    let service: FizzbuzzService = this.owner.lookup('service:fizzbuzz');
    assert.equal(service.execute(5), "Buzz");
  });

  test('it execute fizzbuzz and print FizzBuzz', function(assert) {
    let service: FizzbuzzService = this.owner.lookup('service:fizzbuzz');
    assert.equal(service.execute(15), "FizzBuzz");
  });
});
