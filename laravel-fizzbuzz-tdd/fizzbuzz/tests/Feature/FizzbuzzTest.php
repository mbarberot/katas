<?php

namespace Tests\Feature;

use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class FizzbuzzTest extends TestCase
{

    private function whenPrinting(int $number): TestResponse {
        return $this->get("/api/fizzbuzz/$number");
    }

    private function thenHasResult(TestResponse $response, string $result): void {
        $response->assertStatus(200);
        $response->assertJson(["result" => $result]);
    }

    public function testPrintNumber()
    {
        $response = $this->whenPrinting(1);
        $this->thenHasResult($response, "1");
    }

    public function testPrintFizz()
    {
        $response = $this->whenPrinting(3);
        $this->thenHasResult($response, "fizz");
    }

    public function testPrintBuzz()
    {
        $response = $this->whenPrinting(5);
        $this->thenHasResult($response, "buzz");
    }

    public function testPrintFizzBuzz()
    {
        $response = $this->whenPrinting(15);
        $this->thenHasResult($response, "fizzbuzz");
    }
}
