import Component from '@glimmer/component';
import {tracked} from '@glimmer/tracking';
import {action} from '@ember/object'

export default class FizzbuzzFormComponent extends Component {
  @tracked integer = 0;

  @action
  onSubmit(event) {
    event.preventDefault();

    this.args.onSubmit(this.integer);
  }
}
