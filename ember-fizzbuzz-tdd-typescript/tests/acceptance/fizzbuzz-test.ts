import { module, test } from 'qunit';
import { currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import fizzbuzzPage from 'fizzbuzz/tests/pages/fizzbuzz';

module('Acceptance | fizzbuzz', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /fizzbuzz', async function(assert) {
    await fizzbuzzPage.visit();

    assert.equal(currentURL(), '/fizzbuzz');
    assert.ok(fizzbuzzPage.root)
  });

  test('showing integer', async function(assert) {
    await fizzbuzzPage.visit();

    await fizzbuzzPage.pick(1);

    assert.equal(fizzbuzzPage.result.text, "1");
  });

  test('showing Fizz', async function(assert) {
    await fizzbuzzPage.visit();

    await fizzbuzzPage.pick(3);

    assert.equal(fizzbuzzPage.result.text, "Fizz");
  });

  test('showing Buzz', async function(assert) {
    await fizzbuzzPage.visit();

    await fizzbuzzPage.pick(5);

    assert.equal(fizzbuzzPage.result.text, "Buzz");
  });

  test('showing FizzBuzz', async function(assert) {
    await fizzbuzzPage.visit();

    await fizzbuzzPage.pick(15);

    assert.equal(fizzbuzzPage.result.text, "FizzBuzz");
  });
});
