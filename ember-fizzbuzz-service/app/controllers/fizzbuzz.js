import Controller from '@ember/controller';
import {action} from '@ember/object';
import {tracked} from "@glimmer/tracking";
import {inject as service} from '@ember/service';

export default class FizzbuzzController extends Controller {
  @service fizzbuzz;
  @tracked result = '';
  @tracked submitted = false;

  @action
  onSubmit(value) {
    this.result = this.fizzbuzz.forValue(value);
    this.submitted = true;
  }
}
