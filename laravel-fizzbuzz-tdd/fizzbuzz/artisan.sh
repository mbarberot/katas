#!/bin/bash

docker run \
	-it \
	--rm \
	-v $PWD:/app \
	-w /app \
	--user "$(id -u):$(id -g)" \
	-p 8000:8000 \
	php:7.4-cli \
	php artisan "$@"

exit $?
