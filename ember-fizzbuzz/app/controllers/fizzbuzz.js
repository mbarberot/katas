import Controller from '@ember/controller';
import {action} from '@ember/object';
import {tracked} from "@glimmer/tracking";

export default class FizzbuzzController extends Controller {
  @tracked integer = 0;
  @tracked result = '';
  @tracked submitted = false;

  @action
  onSubmit(event) {
    event.preventDefault();

    let result = '';

    if (this.integer > 0) {
      if (this.integer % 3 === 0) {
        result += 'Fizz';
      }
      if (this.integer % 5 === 0) {
        result += 'Buzz';
      }
    }

    if (result.length === 0) {
      result = `${this.integer}`;
    }

    this.result = result;
    this.submitted = true;
  }
}
