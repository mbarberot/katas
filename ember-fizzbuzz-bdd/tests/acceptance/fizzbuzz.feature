@setupApplicationTest
Feature: fizzbuzz

  Scenario: I can choose a number and I see the result

    Given I access to fizzbuzz
    When I choose 1
    Then I can see 1

    Given I access to fizzbuzz
    When I choose 3
    Then I can see Fizz

    Given I access to fizzbuzz
    When I choose 5
    Then I can see Buzz

    Given I access to fizzbuzz
    When I choose 15
    Then I can see FizzBuzz
