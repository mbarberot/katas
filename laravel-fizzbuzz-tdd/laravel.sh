#!/bin/bash

docker run \
	-it \
	--rm \
	-v $PWD:/app \
	-w /app \
	php:7.4-cli \
	/app/vendor/bin/laravel $*

exit $?
