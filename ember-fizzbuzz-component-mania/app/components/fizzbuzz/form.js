import Component from '@glimmer/component';
import {tracked} from '@glimmer/tracking';
import {action} from '@ember/object'

export default class FizzbuzzFormComponent extends Component {
  @tracked integer = 0;

  @action
  onSubmit(event) {
    event.preventDefault();

    let result = '';

    if (this.integer !== 0) {
      if (this.integer % 3 === 0) {
        result += 'Fizz';
      }
      if (this.integer % 5 === 0) {
        result += 'Buzz';
      }
    }

    if (result.length === 0) {
      result = `${this.integer}`;
    }

    this.args.onSubmit(result);
  }
}
